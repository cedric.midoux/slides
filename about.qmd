---
title: "Cédric Midoux"
image: images/cmidoux.jpg
about:
  template: jolla
  links:
    - icon: twitter
      text: Twitter
      href: https://twitter.com/CedricMidoux
    - icon: book
      text: CV HAL
      href: https://cv.hal.science/cedric-midoux
    - icon: git
      text: GitLab
      href: https://forgemia.inra.fr/cedric.midoux/
---

About Cédric Midoux
