---
title: "Versionner son code grâce à git et partager facilement ces analyses"
description: "R-Antony"
author: "Cédric Midoux"
date: "2022-04-12"
categories: 
  - Git
  - GitLab
  - R
  - workflowr
image: "https://forgemia.inra.fr/cedric.midoux/slides/-/raw/without_quarto/2022-04-12_R-Antony.png"
---

[Liens vers les slides](https://forgemia.inra.fr/cedric.midoux/slides/-/raw/without_quarto/2022-04-12_R-Antony.pdf) de la présentation sur l'utilisation du package `workflowr` au réseau R-Antony.
